<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Policies\PageAccessPolicy;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('adminAccess', User::class);
        $user = User::all();

        return view('admin.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('adminAccess', User::class);
        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
        ]);
        
        $user = new User();
        $user->name = $request->post('name');
        $user->email = $request->post('email'); 
        $user->password = bcrypt($request->post('password')); 
        $user->type = $request->post('type');
        $user->save();

        return redirect()->route('admin.index')->with('success', 'New user has been added');
    }

    /** 
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
            $user=User::find($id);
            return view('admin.edit', compact('user'))->with('success, Information has changed');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);  
        $user->name=$request->post('name');
        $user->email=$request->post('email'); 
        $user->password=bcrypt($request->post('password')); 
        $user->type=$request->post('type');
        $user->save();

        return redirect()->route('admin.index')->with('success', 'New user has been added');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect()->route('admin.index')->with('success','The user has been deleted.');
    }   
}

