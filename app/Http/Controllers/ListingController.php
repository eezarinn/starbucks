<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Listing;
use App\User;
use App\Policies\PageAccessPolicy;

class ListingController extends Controller
{
	public function index() {
		$this->authorize('adminAccess', User::class);
		$listing = Listing::all();

		return view('admin.listing', compact('listing'));
	}

	public function create() {
		$this->authorize('adminAccess', User::class);
		return view('admin.NewList');
	}

	public function store(Request $request) {
		$this->validate($request, [
			'list_name' => 'required',
			'address' => 'required',
			'latitude' => 'required',
			'longitude' => 'required',
			'submitter_id' => 'required'
		]);

		$list = new Listing();
		$list->list_name = $request->input('list_name');
		$list->address = $request->input('address');
		$list->latitude = $request->input('latitude');
		$list->longitude = $request->input('longitude');
		$list->submitter_id = $request->input('submitter_id');
		$list->save();

		return redirect()->route('listing.index');
	}

	public function edit($id){

		$listing = Listing::find($id);
        return view('admin.editList',compact('listing'))->with('success, Information has changed');

	}

	public function update(Request $request, $id){
		$list = Listing::find($id);        
        $list->list_name = $request->post('list_name');
        $list->address = $request->post('address');
        $list->latitude = $request->post('latitude');
        $list->longitude = $request->post('longitude');
        $list->submitter_id = $request->post('submitter_id');
        $list->save();

        return redirect('listing')->with('success, Information has changed');

	}

	public function show(){

	}

	 public function destroy($id)
    {
        $list = Listing::find($id);
        $list->delete();
        return redirect('listing')->with('success','The list has been deleted.');
    }   
}
