<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	[
        		'name'=>'ezarin',
        		'email'=>'ezarin@gmail.com',
        		'password'=>bcrypt(value:'zarin123'),
        		'type'=>'a',
        		'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
        		'updated_at'=>Carbon::now()->format('Y-m-d H:i:s')

        	],

             DB::table('admin')->insert([

        	[
        		'name'=>'sarah',
        		'email'=>'sarah@gmail.com',
        		'password'=>bcrypt(value:'12345678'),
        		'type'=>'u',
        		'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
        		'updated_at'=>Carbon::now()->format('Y-m-d H:i:s')

        	]

        ]);
    }
}
