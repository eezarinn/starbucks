@extends('layouts.app')
@section('content')
	<div class="container">

		 <h2 class="card-title text-info">Create New List</h2>
		<br>
		
		<form method="POST" action="{{route('listing.store')}}" enctype="multipart/form-data">
			@csrf
			<div class="form-group">
				<label for="list_name">List Name: </label>
				<input type="text" class="form-control" name="list_name" id="list_name">
			</div>
			<div class="form-group">
				<label for="address">Address: </label>
				<input type="text" class="form-control" name="address" id="address">
			</div>
			<div class="form-group">
				<label for="latitude">Latitude: </label>
				<input type="number" class="form-control" name="latitude" id="latitude">
			</div>
			<div class="form-group">
				<label for="longitude">Longitude: </label>
				<input type="number" class="form-control" name="longitude" id="longitude">
			</div>

			<input type="hidden" class="form-control" name="submitter_id" id="submitter_id" value="{{ Auth::user()->id }}">

			<div class="fa-pull-right">
				<button type="reset" class="btn btn-outline-info">Reset</button>
				<button type="submit" class="btn btn-info">Submit</button>
			</div>
		</form>
	</div>
@endsection