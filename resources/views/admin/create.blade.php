@extends('layouts.app')
@section('content')

<div class="container">
      <h2 class="card-title text-info">Create New User</h2>
    <br>
    <form method="post" action="{{route('admin.store')}}" enctype="multipart/form-data">
    
    @csrf
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" required>
            </div>
        </div>

        
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" required>
             </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="password">Password</label>
                <input type="text" class="form-control" name="password" required>
             </div>
        </div>


       
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="type">Type</label>
                <input type="text" class="form-control" name="type" required>
             </div>
        </div>
       
             
       

         <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4" style="margin-top:60px">
                <button type="submit" class="btn btn-outline-info">Save</button>
                <a href="{{action('AdminController@index')}}" class="btn btn-info">Back</a>
             </div>
        </div>
    </form>
</div>

@endsection