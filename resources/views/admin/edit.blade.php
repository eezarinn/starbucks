@extends('layouts.app')
@section('content')

<div class="container">
      <h2 class="card-title text-info">Update Users</h2>
    <br>

    <form method="post" action="{{route('admin.update', $user->id)}}" enctype="multipart/form-data">
   
    @csrf
        
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" value="{{$user->name}}"required>
            </div>
        </div>

       <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" value="{{$user->email}}"required>
             </div>
        </div>

       
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="type">Type</label>
                <input type="text" class="form-control" name="type" value="{{$user->type}}"required>
             </div>
        </div>
       
         <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4" style="margin-top:60px">
                <button type="submit" class="btn btn-outline-info">Save</button>
                <a href="{{action('AdminController@index')}}" class="btn btn-info">Back</a>
             </div>
        </div>
        @method('put')
    </form>
</div>


@endsection




