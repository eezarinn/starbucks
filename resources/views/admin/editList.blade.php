@extends('layouts.app')
@section('content')

<div class="container">
      <h2 class="card-title text-info">Update Listing Table</h2>
    <br>

    <form method="post" action="{{route('listing.update', $listing->id)}}" enctype="multipart/form-data">
   
    @csrf
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="list_name">List Name:</label>
                <input type="text" class="form-control" name="list_name" value="{{$listing->list_name}}" required>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="address">Address</label>
                <input type="text" class="form-control" name="address" value="{{$listing->address}}" required>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="latitude">Latitude:</label>
                <input type="text" class="form-control" name="latitude" value="{{$listing->latitude}}"  required>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="longitude">Longitude</label>
                <input type="text" class="form-control" name="longitude" value="{{$listing->longitude}}" required>
             </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="submitter_id">Submitter Id</label>
                <input type="text" name="submitter_id"  class="form-control" value="{{$listing->submitter_id}}">
                
            </div>
        </div>


         <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4" style="margin-top:60px">
                <button type="submit" class="btn btn-outline-info">Save</button>
                <a href="{{action('ListingController@index')}}" class="btn btn-info">Back</a>
             </div>
        </div>
        @method('put')
    </form>
</div>

@endsection