@extends('layouts.app')
@section('content')
    <main role="main" class="container">
        <div class="d-flex align-items-center p-3 my-3 rounded border-bottom">
            <h2 class="card-title text-info">List of Users</h2>
        </div>
            <table class="table">
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Type</th>
                    <th colspan="2">Action</th>
                </tr>
                <tbody>
                @foreach($user as $u)
                <tr>
                    <td>{{$u->name}}</td>
                    <td>{{$u->email}}</td>
                    <td>{{$u->type}}</td>
                    <td>
                        <div class="row">
                            <div class="col-md-2">
                                <a href="{{action('AdminController@edit', $u->id)}}" class="btn btn-warning">Edit</a>
                            </div>
                            <div class="col-md-2">
                                <form action="{{action('AdminController@destroy', $u->id)}}" method="post">
                                    @csrf
                                    <input name="_method" type="hidden" value="DELETE">
                                    <button class="btn btn-info" type="submit">Delete</button>
                                </form>
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </main>
@endsection

 
