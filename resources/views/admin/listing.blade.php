@extends('layouts.app')
@section('content')

   
    <main role="main" class="container">
        <div class="d-flex align-items-center p-3 my-3 rounded border-bottom">
            <h2 class="card-title text-info">Listing&nbsp;<i class="fas fa-hand-holding-heart"></i></h2>
        </div>
        <div class="my-3 p-3 bg-white rounded box-shadow">
            
            <table class="table" style="color: black;">
                <tr>
                   
                    <th>List Name</th>
                    <th>Address</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Submitter Id</th>
                   
                       <th span="2">Action</th>
                </tr>
                @foreach($listing as $l)
                <tr>
                    <td>{{$l->list_name}}</td>
                    <td>{{$l->address}}</td>
                    <td>{{$l->latitude}}</td>
                    <td>{{$l->longitude}}</td>
                    <td>{{$l->submitter_id}}</td>
                    <td>
                        <a href="{{action('ListingController@edit', $l->id)}}" class="btn btn-warning">Edit</a>
                    </td>
                    <td>
                        <form action="{{action('ListingController@destroy', $l->id)}}" method="post">
                            @csrf
                            <input name="_method" type="hidden" value="DELETE">
                            <button class="btn btn-info" type="submit">Delete</button>
                        </form>
                    </td>
                 </tr>
                @endforeach
            </table>
        </div>
    </main>

@endsection

