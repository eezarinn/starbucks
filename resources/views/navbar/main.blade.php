
<nav class="navbar navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg" color-on-scroll="100" id="sectionsNav" href="https://demos.creative-tim.com/material-kit">
  <div class="container">
    <div class="navbar-translate">
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="nav nav-pills">
          <li class="nav-item active">
            <a class="navbar-brand" href="{{url('/home')}}">Starbucks</a>
          </li>
        </ul>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;


        

          
            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Menu</a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="{{route('admin.create')}}">Add New User</a>
                <a class="dropdown-item" href="{{route('admin.index')}}">List of User</a>
                 <a class="dropdown-item" href="{{route('admin.edit')}}">Update Users</a>
            </div>
          </li>
          
    
         

          <li class="nav-item dropdown">
            <ul class="nav nav-pills"> 
              <li class="nav-item active">
                <a class="nav-link dropdown-toggle btn-lg" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-expanded="false">
                  Listing
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" id="navbarDropdownMenuLink"></a>
                   <a class="dropdown-item" href="{{route('admin.NewList')}}">Create New Listing</a>
                  <a class="dropdown-item" href="{{route('admin.listing')}}">The List</a>
                  <a class="dropdown-item" href="{{route('admin.editList')}}">Update Listing</a>
                        
                </div>
              </li>
            </ul>
          </li>
          

           
        </ul>

        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <ul class="navbar-nav ml-auto">
          <!-- Authentication Links -->
         
          @guest
          <li class="nav-item">
              <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
          </li>
            
          @else
          <li class="nav-item dropdown">
              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                  {{ Auth::user()->name }} <span class="caret"></span>
              </a>

              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <div class="divider">
                        <a href="{{route('change.password')}}" class="btn btn-info btn-sm-btn"><i class="fa fa-unlock-alt" aria-hidden="true"></i> Change Password</a>
                    </div>
                  <a class="dropdown-item" href="{{ route('logout') }}"
                     onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                      {{ __('Sign Out') }}
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" class="fa fa-power-off" method="POST" style="display: none;">
                      @csrf
                  </form>
            </div>
          </li>
          @endguest
        </ul>
      </div>
    </div>
  </div>
</nav>
  

                                      