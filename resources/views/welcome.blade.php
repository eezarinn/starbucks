<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Starbucks</title>

        <!-- Bootstrap & font awesome Css ---->
  
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
        <link href="{{ asset('css/bootstrap-grid.css') }}" rel="stylesheet">
        <link href="{{ asset('css/bootstrap-reboot.css') }}" rel="stylesheet">
        <link href="{{ asset('css/fontawesome.css') }}" rel="stylesheet">
        <link href="{{ asset('css/all.css') }}" rel="stylesheet">
        <link href="{{ asset('css/brands.css') }}" rel="stylesheet">
        <link href="{{ asset('css/regular.css') }}" rel="stylesheet">
        <link href="{{ asset('css/solid.css') }}" rel="stylesheet">
        <link href="{{ asset('css/svg-with-js.css') }}" rel="stylesheet">
        <link href="{{ asset('css/v4-shims.css') }}" rel="stylesheet">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
   

        <!-- Google fonts --->
        <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
       
        <!-- Custom Css ---->
        <link href="{{ asset('css/welcome.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="style.css">


        <!-- Fevicon Add ---->
        <link rel="icon" href="img/fevicon.ico" type="image/ico" sizes="16x16">
         <!-- Styles -->
  <style>
                html, body {
                    background-color: #fff;
                    color: #ffffff;
                    text-shadow: #000000
                    font-family: 'Quicksand', sans-serif;
                    font-weight: 100;
                    height: 100vh;
                    margin: 0;ss
                    background-repeat: repeat;
                    background-size: cover;
                    background-position: center center;
                 
                    background-image: url(img/image1.jpg);
                }
                .full-height {
                    height: 100vh;
                }

                .flex-center {
                    align-items: center;
                    display: flex;
                    justify-content: center;
                }

                .position-ref {
                    position: relative;
                }

                .top-right {
                    position: absolute;
                    right: 10px;
                    top: 18px;
                }

                .content {
                    text-align: center;
                }

                .title {
                    font-size: 84px;
                    font: Quicksand;
                    background: rgba(140, 140, 140, 0.4);

                }


                .m-b-md {
                    margin-bottom: 30px;
                  }

  </style>

    </head>
    <body>

        <body id="top"><div class="site-wrapper">
          <div class="site-wrapper-inner">
            <div class="cover-container">
             <div class="masthead clearfix">
              <div class="inner">
                <h3 class="masthead-brand">Starbucks</h3>
                <nav class="nav nav-masthead">
                  <a class="nav-link nav-social" href="https://www.facebook.com"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                  <a class="nav-link nav-social" href="https://twitter.com/"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                  <a class="nav-link nav-social" href="https://mail.google.com/"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                 
                </nav>
              </div>
            </div>
            <br>
            <div class="inner cover">
              <h1 class="cover-heading">WELCOME TO</h1>
              <h5 class="sub-header"> THIS SYSTEM </h5>
              <br>
            
      
                  @if (Route::has('login'))
                     
                            @auth
                            @else
                                <a href="{{ route('login') }}"><button type="button" class="btn btn-lg btn-default btn-notify" data-toggle="modal" data-target="#subscribeModal">SIGN IN</button>
                                
                            @endauth
                        </div>
                    @endif

                    @if (Route::has('login'))
                                
                                    @auth
                                        <a href="{{ url('/home') }}" class="h3">Home</a>
                                    @else
                                    @endauth
                               
                            @endif
              
      <div class="mastfoot">
        <div class="inner">
            <p>©Starbucks. Design Made By Ezarin</p>
        </div>
      </div>
  </div>
</div>


    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
         <!-- <br>
            <div class="inner cover">
              <h1 class="cover-heading">WELCOME TO</h1>
              <h5 class="sub-header"> THIS System</h5>
              <br>
          </div>

        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Welcome :)
                </div> -->

               

                </div>
            </div>
        </div>
    </body>
</html>
